import { Component, HostListener, ViewChild, Input, ElementRef } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  @Input('class') robotOrientation = 'robot-up';
  x: number = 3;
  y: number = 3;

  @HostListener('document:keypress', ['$event'])
  handleKeyboardEvent(event: KeyboardEvent) {
    let keypress = event.key.toUpperCase(); 
    switch(keypress) {
      case 'I':
        this.robotOrientation = 'robot-up';
        break;
      case 'K':
        this.robotOrientation = 'robot-down';
        break;
      case 'J':
        this.robotOrientation = 'robot-left';
        break;
      case 'L':
        this.robotOrientation = 'robot-right';
        break;
      case 'S':
        // move robot
        switch(this.robotOrientation) { // find orientation of robot before moving it one grid
          case 'robot-up':
            if (this.x > 1) {
              this.x--;
            }
            break;
          case 'robot-down':
            if (this.x < 5) {
              this.x++;
            }
            break;
          case 'robot-left':
            if (this.y > 1) {
              this.y--;
            }
            break;
          case 'robot-right':
            if (this.y < 5) {
              this.y++;
            }
            break;
        }
        console.log(this.x + ',' + this.y);
    }
  }
}

